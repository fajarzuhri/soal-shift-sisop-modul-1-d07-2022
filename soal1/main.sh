# DISPLAY WELCOME
echo "LOGIN TO MY SYSTEM"

# User input their username and password
read -p "Username : " username
read -s -p "Password : " password

echo

# Program will try to find the username and password in ./user/user.txt
x=`awk "/^$username $password$/" ./user/user.txt`

# If its not exist, then login error
if [ "$x" == "" ]; then
	# Display error message
	echo "Login Failed"

	# Add log message to log.txt
	echo `date +'%m/%d/%Y %H:%M:%S'` "LOGIN: ERROR Failed login attempt on user $username" >> log.txt

# Login succeed
else
	# Add log message
	echo `date +'%m/%d/%Y %H:%M:%S'` "LOGIN: INFO User $username logged in" >> log.txt

	# User give command
	read command

	# If user type command att
	if [ "$command" == "att" ]; then
		# Program will find succeessfull login attempt on that user, based on log.txt
		awk "BEGIN {print \"Successfull login attempt :\" } /LOGIN: INFO [a-zA-Z0-9 ]+ $username / {++n} END {print n}" log.txt

		# Program will also find failed login attempt on that user, based on log.txt
		awk "BEGIN {print \"Failed login attempt      :\" } /LOGIN: ERROR [a-zA-Z0-9 ]+ $username$/ {++n} END {print n}" log.txt

	# If user type command dl [num]
	elif [[ "$command" =~ ^dl\ [0-9]+$ ]]; then
		# Program will get the number
		IFS=' ' read -r -a array <<< "$command"
		count=${array[-1]}

		# Define foldername using a certain format
		foldername="`date +'%Y-%m-%d'`_$username"

		# Initialize start number of filename pattern
		start=1

		if [ -f "$foldername.zip" ]; then
			# If a zip file with a given name exist, then unzip first using user password
			unzip -P $password "$foldername.zip"

			# Then go to that folder
			cd $foldername

			# Get total current file and add for start filename pattern
			start=`ls | wc -l`
			start=$start+1

			# Go to outside
			cd ../
		fi

		# Create folder with a given name if it does not exist yet
		mkdir -p  $foldername

		# Go to that folder
		cd $foldername

		# For a certain amount, download file from a given link with such pattern filename
		for ((num=start; num<start+count; num=num+1)); do
			curl https://loremflickr.com/320/240 -L > PIC_`printf "%02d" $num`.jpg
		done

		# Go to outside
		cd ../

		# Zip that folder and protect it with user password
		zip --password $password -r "$foldername.zip" $foldername

		# Then, remove the folder
		rm -r $foldername
	fi
fi
