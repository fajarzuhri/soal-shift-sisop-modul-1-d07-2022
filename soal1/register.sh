# DISPLAY WELCOME
echo "WELCOME TO MY SYSTEM"
echo "Register to my system"

# Using while true break
while true; do
	# User input desired username
	read -p "Username : " username

	# Program will try to find that username in ./user/user.txt
	x=`awk "/^$username /" ./user/user.txt`

	if [ "$x" != "" ]; then
		# If user is exist, program will error
		echo "ERROR : user $username already exist"

		# Program will add this to log
		echo `date +'%m/%d/%Y %H:%M:%S'` "REGISTER: ERROR User already exists" >> log.txt

		# Then back to input username
		continue
	else
		# If user does'nt exist yet, program will ask the password for this user
		read -s -p "Password : " password

		echo

		# Password cannot be shorter than 8 character
		if [ ${#password} -lt 8 ]; then
			echo "Error : Password must be minimum 8 character"
			continue

		# Password cannot be the same as username
		elif [ "$password" == "$username" ]; then
			echo "Error : Password cannot be the same as username"
			continue

		# Password must contain minimum 1 uppercase character and 1 lowercase character
		elif [[ ! ("$password" =~ [A-Z] && "$password" =~ [a-z]) ]]; then
			echo "Error : Password must contain minimum 1 uppercase and 1 lowercase"
			continue

		# Password acceptable
		else
			echo "User $username registered successfully"

			# Program will add log message
			echo `date +'%m/%d/%Y %H:%M:%S'` "REGISTER: INFO User $username registered successfully" >> log.txt

			# Save username and password to ./user/user.txt
			echo "$username $password" >> ./user/user.txt
			break
		fi
	fi
done
