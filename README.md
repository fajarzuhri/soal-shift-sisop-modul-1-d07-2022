# soal-shift-sisop-modul-1-D07-2022

Praktikum 1 Sistem Operasi 2022

## Anggota Kelompok
- Fitra Agung Diassyah Putra - 5025201072
- Ananda Hadi Saputra - 5025201148
- Fajar Zuhri Hadiyanto - 5025201248

## Soal 1
### Eksekusi Script
![Gambar 1](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-1/Screenshot+2022-02-26+202328.png)

Gambar di atas menunjukkan proses register ke dalam sistem, dimana user diminta memasukkan username dan password dengan kriteria tertentu.

![Gambar 2](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-1/Screenshot+2022-02-26+202423.png)

Gambar di atas menunjukkan proses login dan melihat jumlah percobaan login baik yang berhasil ataupun gagal.

![Gambar 3](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-1/Screenshot+2022-02-26+202510.png)

Gambar di atas menunjukkan proses login ke dalam sistem dan download gambar dari url tertentu, dimana file2 tersebut akan dizip dengan password yang dimiliki oleh user.

### Hasil
![Gambar 4](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-1/Screenshot+2022-03-05+213101.png)

Gambar di atas menunjukkan isi dari file log.txt yang berisi log dari setiap percobaan register dan login ke dalam sistem, baik yang berhasil maupun gagal.

![Gambar 5](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-1/Screenshot+2022-03-05+213128.png)

Gambar di atas menunjukkan isi dari file ./user/user.txt yang berisi list dari user beserta password yang terdaftar dalam sistem.

![Gambar 6](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-1/Screenshot+2022-03-05+213220.png)

Gambar di atas menunjukkan list dari file image yang telah didownload dengan format nama tertentu di dalam folder dengan format nama tertentu.

### Kendala

Untuk soal 1, terdapat satu kendala, yaitu beberapa best practice dan tips yang tidak secara langsung diajarkan di sesi lab, seperti regular expression dan penggunaan date dengan format tertentu. Jadi untuk mengerjakan soal 1, perlu banyak mengeksplor command-command dan juga best practice tertentu pada linux. Tetapi soal 1 dapat diselesaikan sebelum memasuki sesi revisi.

## Soal 2
### Eksekusi Script
![Gambar 7](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-1/sisop-modul-1-soal-2/1646487764989.jpg)

Gambar di atas menunjukkan eksekusi file shell script. Tidak terdapat message apapun, termasuk message error.

### Hasil
![Gambar 8](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-1/sisop-modul-1-soal-2/1646487547235.jpg)

Gambar di atas menunjukkan isi dari file ratarata.txt yang berisi rata-rata request dalam rentang waktu 12 jam, yaitu 81 reuqest per jam.

![Gambar 9](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-1/sisop-modul-1-soal-2/1646487576037.jpg)

Gambar di atas menunjukkan isi dari file result.txt yang berisi beberapa informasi yaitu :
- ip terbanyak yang melakukan request
- jumlah request yang menggunakan curl
- ip yang mengakses server pada jam 2

### Kendala
Dalam mengerjakan soal ini, terdapat kesulitan yaitu untuk memanfaatkan utilitas awk dan menggabungkannya dengan beberapa command tertentu. Perlu beberapa waktu untuk eksplorasi, seperti perhitungan ip terbanyak yang menggunakan command awk, sort, uniq, head, dan beberapa command lainnya. Diperlukan 1 hari sesi revisi untuk menyelesaikan bagian tersebut.

## Soal 3
### Eksekusi Script

![Gambar 10](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-1/sisop-modul-1-soal-3/Screenshot+from+2022-03-06+12-52-18.png)

Gambar di atas menunjukkan hasil dari eksekusi file minute_log.sh

![Gambar 11](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-1/sisop-modul-1-soal-3/Screenshot+from+2022-03-06+12-52-39.png)

Gambar di atas menunjukkan hasil dari eksekusi file aggregate__minutes_to_hourly_log.sh

### Hasil

![Gambar 12](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-1/sisop-modul-1-soal-3/Screenshot+from+2022-03-06+12-52-50.png)

Gambar di atas menunjukkan adanya file log yang berhasil di generate dari eksekusi file sebelumnya

### Kendala

Kendala dalam mengerjakan soal ini yaitu sering terjadi kesalahan dalam menerapkan command pada shell script sehingga tidak menghasilkan output yang diinginkan. Walaupun begitu, soal ini dapat diselesaikan sebelum sesi revisi.
