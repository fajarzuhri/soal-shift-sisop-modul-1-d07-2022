#!/bin/bash
mkdir -p forensic_log_website_daffainfo_log
touch ratarata.txt
touch result.txt

#Function untuk Menghitung rata rata dari request yang masuk per jam
function rataratareq(){

#Menghitung total line
line=`wc -l < log_website_daffainfo.log`
jam=12

#total dari semua line dibagi oleh lamanya request berjalan 
bagi=`expr $line / $jam `

#print hasil dari rata rata request dan memasukkan hasil kedalam file ratarata.txt
echo "Rata Rata serangan adalah sebanyak $bagi request perjam" >> forensic_log_website_daffainfo_log/ratarata.txt
}

#function untuk menghitung IP yang melakukan request terbanyak
function cekIpTerbanyak(){

	#menghitung banyaknya IP 216.93.144.47 dan memasukkannya ke dalam file result.txt
        baris=`awk -F':' '{print $1}' log_website_daffainfo.log |sort| uniq -c | sort -rn | head -n 1`
        IFS=' ' read -r -a array <<< "$baris"
        ip=${array[1]}
        jumlah=${array[0]}
        echo "IP pengakses paling banyak yaitu : $ip mengakses sebanyak $jumlah kali" >> forensic_log_website_daffainfo_log/result.txt
#        echo >> forensic_log_website_daffainfo_log/result.txt

}

#function untuk menhitung requst yang menggunakan user agent-curl\
function cekCurl(){

	#menghitung banyaknya request yang menggunakan curl dan memasukkannya ke dalam file result.txt
	awk ' 
		/curl/ {++n} 
		END {print "ada", n, "request yang menggunakan curl user-agent"} ' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
}

#function untuk menghitung request yang terjadi pada jam 2 dan memasukkan ke dalam file result.txt
function printJam(){

	#program menampilkan IP dan jam request 
	awk -F':' '/22\/Jan\/2022:02:/ {print $1,"jam", $3, "pagi"}' log_website_daffainfo.log | uniq >> forensic_log_website_daffainfo_log/result.txt

}

#deploy semua function
rataratareq
cekIpTerbanyak
cekCurl
printJam
